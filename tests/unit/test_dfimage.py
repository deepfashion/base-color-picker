import pytest

from picker import dfimage

@pytest.fixture
def image_instance():
    return dfimage.DFImage('./artworks/speaker.jpg')


def test_width(image_instance):
	width = image_instance.width
	assert width == 729

def test_height(image_instance):
	height = image_instance.height
	assert height == 619

def test_raw_colors(image_instance):
	raw_colors = image_instance.raw_colors
	assert len(raw_colors) == 1365

@pytest.mark.parametrize('format_', ['rgb', 'hex', 'name'])
def test_colors(image_instance, format_):
	rv = image_instance.colors(format_ = format_)
	assert rv