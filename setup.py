import io
import os

from setuptools import setup, find_packages


here = os.path.abspath(os.path.dirname(__file__))

# Avoids IDE errors, but actual version is read from version.py
__version__ = None
exec(open('picker/version.py').read())

short_description = 'Deep fashion middleware to extract image colors.'

# Get the long description from the README file
with io.open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

install_requires = [
    'Pillow>=5.4.1',
    'webcolors>=1.8.1',
]

extras_requires = {
    'tests': [
        'coverage >= 4.3.4',
        'codecov >= 2.0.15',
        'pytest >= 3.0.3',
        'pytest-cov >= 2.4.0',
        'flake8 >= 3.6.0',
        'flake8_docstrings >= 1.0.2'],
}


setup(
    name="Base-Color-Picker",
    version=__version__,
    author="Bo",
    author_email="kingbolanda@live.com",
    description=(short_description),
    license="Apache 2.0",
    url="https://bitbucket.org/deepfashion/base-color-picker/src/master/",
    packages=find_packages(),
    long_description=long_description,
    long_description_content_type='text/markdown',
    classifiers=[
        "Development Status :: 3 - Alpha",
        'Environment :: Console',
        'Operating System :: POSIX :: Linux',
        'Topic :: Scientific/Engineering :: Artificial Intelligence',
        "License :: OSI Approved :: Apache Software License",
        'Programming Language :: Python :: 3.6'
    ],
    install_requires=install_requires,
    extras_require=extras_requires
)
