"""Extract color from image."""

import typing

from PIL import Image
from webcolors import rgb_to_name


class DFImage(object):
    """DFImage is the deep fashion middlewhale for color extraction.

    It supports extract images and return rgb, hex or color by name.
    """

    def __init__(
        self,
        file_instance: typing.Union[Image.Image, str],
    ):
        """Initialization."""
        self._img = file_instance if isinstance(
            file_instance, Image.Image) else Image.open(file_instance)
        if self._img.mode not in ('RGB', 'RGBA', 'RGBa'):
            self._img = self._img.convert('RGB')
        self._raw_colors = self._img.getcolors(self.width * self.height)

    @property
    def width(self) -> int:
        """Get the width of the image.

        :return: Width as int.
        """
        return self._img.size[0]

    @property
    def height(self) -> int:
        """Get the height of the image.

        :return: Height of the image.
        """
        return self._img.size[1]

    @property
    def raw_colors(self) -> list:
        """
        Extracted raw colors.

        List of tuples, indicates number of pixels and pixel RGB.

        :return: List of tuples.
        """
        return self._raw_colors

    def colors(self, number: int = 5, format_: str = 'name') -> list:
        """
        Get colors and it's proporation.

        :param number: Top n numbers to extract.
        :param format_: Expected output format, such as `rgb`, `name`
            or `hex`.

        :return rv: List of colors and their percentage.
        """
        rv = []
        if number > len(self._raw_colors):
            raise IndexError(
                'Number to extract exceeds the max number of color in image.'
            )
        if format_ not in ['rgb', 'name', 'hex']:
            raise ValueError('Format should be one of `rgb`, `name` or `hex`.')
        raw_color_sorted = sorted(
            self._raw_colors,
            key=lambda tup: tup[0],
            reverse=True
        )[:number]
        total_pixels = float(self.width * self.height)
        if format_ == 'name':
            for num_pixel, rgb_tuple in raw_color_sorted:
                prop = num_pixel / total_pixels
                try:
                    name = rgb_to_name(rgb_tuple)
                except ValueError:
                    name = None
                rv.append((prop, name))
        elif format_ == 'hex':
            rv = [(num_pixel / total_pixels, self._rgb_to_hex(rgb_tuple))
                  for
                  num_pixel, rgb_tuple
                  in
                  raw_color_sorted]
        else:
            rv = [(num_pixel / total_pixels, rgb_tuple)
                  for
                  num_pixel, rgb_tuple
                  in
                  raw_color_sorted]
        return rv

    def _rgb_to_hex(self, rgb_tuple: tuple) -> str:
        r, g, b = rgb_tuple
        return "#{:02x}{:02x}{:02x}".format(r, g, b)
