# Base Color Picker

> Deep fashion middleware to extract image colors.

| [![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)  |  [![Build Status](https://img.shields.io/bitbucket/pipelines/deepfashion/base-color-picker.svg)](https://bitbucket.org/deepfashion/base-color-picker/addon/pipelines/home#!/) |  [![codecov](artworks/coverage.svg)]() |
---

## Get Started

```python
from picker.dfimage import DFImage
img_dir = 'your-image-dir'
img_instance = DFImage(img_dir)
# Get image size.
img_width = img_instance.width
img_height = img_instance.height
# Get raw colors (number of pixels and corresponded rgb).
img_raw_colors = img_instance.raw_colors
# Get dominant 1 colors by format.
img_color_rgb = img_instance.colors(number=1, format_='rgb')
# >>> (0.36, (255, 255, 255))
img_color_hex = img_instance.colors(number=1, format_='hex')
# >>> (0.36, '#ffffff')
img_color_name = img_instance.colors(number=1, format_='name')
# >>> (0.36, 'black')
```

## Install

```
cd base-color-picker
python setup.py install
```

## Test

```
make init
make test
```

## Contact

kingbolanda@live.com

