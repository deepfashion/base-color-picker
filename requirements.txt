Pillow>=5.4.1
webcolors>=1.8.1
coverage >= 4.3.4
codecov >= 2.0.15
pytest >= 3.7.4
pytest-cov >= 2.4.0
flake8 >= 3.6.0
flake8_docstrings >= 1.3.0
